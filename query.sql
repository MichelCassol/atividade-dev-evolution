
DROP TABLE produtos;

CREATE TABLE produtos (
    id INTEGER PRIMARY KEY AUTOINCREMENT
    ,nome VARCHAR NOT NULL
    ,preco FLOAT NOT NULL
    ,data_criacao VARCHAR NOT NULL
    ,data_atualizacao VARCHAR
);

