<?php 
function menu() 
{
    system('clear');
    echo "1 - Cadastrar um produto" . PHP_EOL;
    echo "2 - Listar todos os produtos" . PHP_EOL;
    echo "3 - Listar um registro pelo ID" . PHP_EOL;
    echo "4 - Atualizar um produto" . PHP_EOL;
    echo "5 - Deletar um produto" . PHP_EOL;
    echo "6 - Deletar todos os produtos" . PHP_EOL;
    echo "0 - Sair" . PHP_EOL;
    echo PHP_EOL;
}

function insertProduto(SQLite3 $db)
{
    $nome = readline("Informe o nome do produto: ");
    $preco = readline("Informe o preço do produto: ");
    $data =  date('Y-m-d H:i:s');

    if ($nome && $data && is_numeric($preco)) {
        try {
            $sql = "INSERT INTO produtos (nome,preco,data_criacao,data_atualizacao) VALUES(:nome, :preco, :data_criacao, :data_atualizacao)";

            $stmt = $db->prepare($sql);
            $stmt->bindValue(':nome', $nome);
            $stmt->bindValue(':preco', $preco);
            $stmt->bindValue(':data_criacao', $data);
            $stmt->bindValue(':data_atualizacao', $data);
            
            $stmt->execute();
            
            echo "Produto cadastrado!" . PHP_EOL;
            sleep(2);
        } catch (SQLite3Exception $e) {
            echo "Erro ao cadastrar o produto" . PHP_EOL;
            sleep(2);
        }
    }else {
        echo "Dados inválidos!" . PHP_EOL;
        sleep(2);
    }
}

function listarProdutosAll(SQLite3 $conexao)
{
    try {
        $resultado = $conexao->query("SELECT * FROM produtos");

        while ($result = $resultado->fetchArray()) 
        {
            echo PHP_EOL . "ID: " . $result['id'] .
            "           ==> Produto: " . $result['nome'] .
            "           ==> Preço: " . $result['preco'] . 
            "           ==> Data cadastro: " . $result['data_criacao'] .
            "           ==> Data ultima atualização: " . $result['data_atualizacao'] .
            PHP_EOL;
        }
    } catch (SQLite3Exception $th) {
        echo "Erro ao consultar o banco de dados!";
    }
    echo PHP_EOL;
    readline("Pressione enter para retornar ao menu");
}

function consultaProduto(SQLite3 $conexao)
{
    $id = readline("Informe o ID do produto: ");

    while (!is_numeric($id)) {
        $id = readline("ID inválido, informe o ID do produto: ");
    }

    try {
        $stmt = $conexao->prepare("SELECT * FROM produtos WHERE id = :id");
        $stmt->bindValue(':id', $id, SQLITE3_INTEGER);
        $result = $stmt->execute();
        $registro = $result->fetchArray(SQLITE3_ASSOC);

        if ($registro) {
            echo PHP_EOL . "Produto: " . $registro['nome'] .
                "   ==> Preço: " . $registro['preco'] .
                "   ==> Data cadastro: " . $registro['data_criacao'] .
                "   ==> Data ultima atualização: " . $registro['data_atualizacao'] .
                PHP_EOL;
        } else {
            echo "Registro não encontrado!";
        }
    } catch (SQLite3Exception $th) {
        echo "Erro ao consultar o banco de dados!";
    }
    
    echo PHP_EOL;
    readline("Pressione enter para retornar ao menu");
}

function atualizaProduto(SQLite3 $conexao)
{
    $id = readline("Informe o ID do produto: ");

    while (!is_numeric($id)) {
        $id = readline("ID inválido, informe o ID do produto: ");
    }

    $stmt = $conexao->prepare("SELECT * FROM produtos WHERE id = :id");
    $stmt->bindValue(':id', $id, SQLITE3_INTEGER);
    $result = $stmt->execute();
    $registro = $result->fetchArray(SQLITE3_ASSOC);

    if ($registro) {
        $nome = readline("Informe o nome do produto: ");
        $preco = readline("Informe o preço do produto: ");
        $data =  date('Y-m-d H:i:s');

        if ($nome && $data && is_numeric($preco)) {
            try {
                $stmt = $conexao->prepare("UPDATE produtos SET nome = :nome, preco = :preco, data_atualizacao = :data_atualizacao WHERE id = :id");
                $stmt->bindValue(':id', $id, SQLITE3_INTEGER);
                $stmt->bindValue(':nome', $nome);
                $stmt->bindValue(':preco', $preco);
                $stmt->bindValue(':data_atualizacao', $data);
                $stmt->execute();
                echo "Produto atualizado!";
                sleep(2);
            } catch (SQLite3Exception $th) {
                echo "Erro ao consultar o banco de dados!";
                sleep(2);
            }
        }else {
            echo "Dados inválidos!" . PHP_EOL;
            sleep(2);
        }
    } else {
        echo "Registro não encontrado!";
        sleep(2);
    }
}

function deletaProduto(SQLite3 $conexao)
{
    $id = readline("Informe o ID do produto: ");

    while (!is_numeric($id)) {
        $id = readline("ID inválido, informe o ID do produto: ");
    }

    try {
        $stmt = $conexao->prepare("DELETE FROM produtos WHERE id = :id");
        $stmt->bindValue(':id', $id, SQLITE3_INTEGER);
        $stmt->execute();

        $registro = $conexao->changes();

        if ($registro > 0){
            echo "Produto excluído!";
            sleep(2);
        } else {
            echo "Registro não encontrado!";
            sleep(2);
        }
    } catch (SQLite3Exception $th) {
        echo "Erro ao excluir o produto!";
    }
}

function deletarTodosProdutos(SQLite3 $conexao)
{
    try {

        $conexao->query("DELETE FROM produtos");
        $conexao->query("VACUUM");

        $registro = $conexao->changes();

        if ($registro > 0){
            echo "Produtos excluídos!";
            sleep(2);
        } else {
            echo "Nenhum registro encontrado!";
            sleep(2);
        }
    } catch (SQLite3Exception $th) {
        echo "Erro ao excluir os produtos!";
    }
}

$db = new SQLite3("database.sqlite");

while (true) 
{
    menu();
    $opcao = readline("Informe a opção: ");
    switch ($opcao) {
        case 1:
            insertProduto($db);
            break;
        
        case 2:
            listarProdutosAll($db);
            break;

        case 3:
            consultaProduto($db);   
            break;

        case 4:
            atualizaProduto($db);
            break;
        
        case 5:
            deletaProduto($db);
            break;
        
        case 6:
            deletarTodosProdutos($db);
            break;

        case null:
            break;

        case 0:
            system('clear');
            exit;

        default:
            echo "Opção inválida!" . PHP_EOL;
            sleep(2);
            echo PHP_EOL;
            break;
    }
}


